# README #

http://ciderpunk.itch.io/space-courier

The culmination of a month of off and on (mostly off) work and then a week of extreme grind!

Featuring 2D art from Lando of Decadence Comics

Navigate caves in your space ship to make deliveries ...space deliveries

Three (3!!!) different levels
physics
explosions
Keith, the reptile
Ed, the brain thing
Remote Opposition Disassembly Device
SVG level format editable in Inkscape
What's missing

Lando made some awesome alien comms chatter sounds that I didn't have time to add
A start screen
any sort of menu
a lot of levels and story progression
WebGL version - I used some functions the GWT didn't like :(
Possible android version, (with on screen controls ..yuck)
better targeting gun-bots
Known issues

Occasionally radio chatter pics don't appear, restarting the game fixes that
dialogue text content is clipped either side in some screen configurations, full screen mostly, (any suggestions?)
Look for some updates later in the week adding more stuff!

Apologies for the screenshots, it looks better in action! will add more tomorrow with a devlog.

Source code is here:

https://bitbucket.org/CiderPunk/spacecourier/

Controls

Thrust with the up arrow cursor key
Turn the ship with left and right cursor keys
Trigger the tether with left alt
Fire with space

Other Assets used

Big thanks to these guys for their open source sounds / textures / fonts

Rubberduck
http://opengameart.org/content/50-free-textures-4-...

Michael Kurinnoy
http://opengameart.org/content/space-battle-game-s...

lolamadeus
https://www.freesound.org/people/lolamadeus/sounds...
https://www.freesound.org/people/lolamadeus/sounds...
https://www.freesound.org/people/lolamadeus/sounds...

Matt McInerey
https://www.google.com/fonts/specimen/Orbitron