package net.ciderpunk.spacetrucker;

/**
 * Created by Matthew on 11/01/2016.
 */
public class Constants {
  public static final boolean ClearKeyConfig = true;
  public static final String KeyPreferencesPath = "SpaceCourier.keys";


  public static final String AtlasPath = "res.atlas";
  public static final float PlayerMaxHealth = 100f;
  public static final float PlayerBulletDamage = -20f;
  public static final float PlayerDeathTime = 5f;




  public static final short World_Fixture = 1;
  public static final short Player_Fixture = 2;
  public static final short Enemy_Fixture = 4;
  public static final short Bullet_Fixture = 8;
  public static final short Sensor_Fixture = 16;
  public static final short Cargo_Fixture = 32;

  public static final float TetherRange = 20f;
  //range squared for some calcs.
  public static final float TetherRange2 = TetherRange * TetherRange;

  public static final float SimulationSpeed = 1/60f;
}
