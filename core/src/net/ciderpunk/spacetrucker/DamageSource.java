package net.ciderpunk.spacetrucker;

import com.badlogic.gdx.math.MathUtils;

/**
 * Created by Matthew on 15/01/2016.
 */
public enum DamageSource {
  Environment("Environment", new String[]{"a wall or floor or something stupid", "gravity", "incompetence", "stupidity"}),
  Player("Player", new String[]{"You! you killed you?!?!  HOW?"}),
  RunAway("RunAway", new String[]{"running away", "agoraphobia",  "irreconcilable guilt", "wanderlust", "curiosity, like that cat, what was his name?"}),

  GunBot("GunBot", new String[]{"getting perforated by a Gunbot", "getting shot a lot by a Gunbot", "staying still too long"}),
  ;

  private String[] messages;
  public final String name;
  DamageSource(String name, String[] messages){
    this.name = name;
    this.messages = messages;
  }

  public String getMessage(){
    return this.messages[MathUtils.random(this.messages.length-1)];

  }


}
