package net.ciderpunk.spacetrucker;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.*;
import net.ciderpunk.spacetrucker.controls.ActionMap;
import net.ciderpunk.spacetrucker.ents.blufor.Player;
import net.ciderpunk.spacetrucker.ents.cargo.Crate;
import net.ciderpunk.spacetrucker.ents.decoration.Explosion;
import net.ciderpunk.spacetrucker.ents.missile.Bullet;
import net.ciderpunk.spacetrucker.ents.opfor.GunBot;
import net.ciderpunk.spacetrucker.gui.*;
import net.ciderpunk.spacetrucker.level.DialogueDef;
import net.ciderpunk.spacetrucker.level.Level;
import net.ciderpunk.spacetrucker.levels.*;
import net.ciderpunk.spacetrucker.map.Map;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.IResourceWatcher;
import net.ciderpunk.spacetrucker.resources.ResourceManager;
import net.ciderpunk.spacetrucker.sound.SoundManager;


public class SpaceCourier extends ApplicationAdapter implements InputProcessor, IResourceWatcher {


  final Level[] levels = new Level[]{
			new Level("levels/level4.svg", "levels/level4.json"),
      new Level1(),
      new Level2(),
      new Level3(),
  };


  int currentLevel = 0;
	SpriteBatch spriteBatch;
	Viewport view;
  Table baseTable;
	Map map;
  ResourceManager resMan;
  Stage stage;
  DeathMessage deathMessage;
  PauseMessage pauseMeesage;
  SuccessMessage successMessage;
  Timer timer;

  HealthBar healthBar;


  RadioChat radioChat;

  public static final  ActionMap keymap  = new ActionMap();

	private static final Vector2 temp = new Vector2();
  private Skin skin;


  @Override
	public void resize(int width, int height) {
		view.update(width,height);
		//cam.position.set(cam.viewportWidth/2,cam.viewportHeight/2,0);
	}

	@Override
	public void create () {

    //Gdx.input.setInputProcessor(keymap);
    Gdx.app.log("game", "started");
    //setup 2d view for gui and crap
		view = new FitViewport(800,600);

    spriteBatch = new SpriteBatch();

    stage = new Stage(view, spriteBatch);

    //stage.setDebugAll(true);
    //gui table
    baseTable = new Table();
    baseTable.setFillParent(true);
    //baseTable.setDebug(true);

    this.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

    stage.addActor(baseTable);


    InputMultiplexer multiplexer = new InputMultiplexer();
    multiplexer.addProcessor(stage);
    multiplexer.addProcessor(keymap);
    Gdx.input.setInputProcessor(multiplexer);

    resMan = new ResourceManager(this);

	}

	@Override
	public void render () {
    if (resMan.update()) {

      map.update();
      stage.act();


      Gdx.gl.glClearColor(0.1f, 0.1f, 0.15f, 1);
      Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
      map.draw();

      stage.draw();
    }
	}


	@Override
	public void dispose() {
		super.dispose();
    map.dispose();
		spriteBatch.dispose();
    skin.dispose();
    resMan.dispose();
    keymap.saveConfig(Constants.KeyPreferencesPath);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    view.unproject(temp.set(screenX, screenY));
    map.createTestBody(temp);
		return true;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}




  @Override
  public void loadComplete() {

    reset();
    //load key config
    if (Constants.ClearKeyConfig) {
      keymap.loadDefaultConfig();
    } else {
      keymap.loadConfig(Constants.KeyPreferencesPath);
    }
  }


  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.addResourceUser(SoundManager.instance);
    resMan.addResourceUser(new IResourceUser[]{new Player.Loader(), new Explosion.Loader(), new Bullet.Loader(), new RadioChat.Loader(), new Crate.Loader(), new GunBot.Loader()});
    resMan.load(Constants.AtlasPath, TextureAtlas.class);
    resMan.getAssetMan().load("res.json", Skin.class);
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    skin = resMan.getAssetMan().get("res.json", Skin.class);


    timer = new Timer(this, skin);
    baseTable.add(timer).top().left().pad(20);
    healthBar = new HealthBar( skin);
    baseTable.add(healthBar).top().right().pad(20);



    deathMessage = new DeathMessage(skin, this);
    successMessage = new SuccessMessage(skin, this);
    pauseMeesage = new PauseMessage(skin, this);
    radioChat = new RadioChat( skin);
    baseTable.row();
    baseTable.add(radioChat).expand().bottom().left().pad(20);

    //baseTable.setDebug(true);
  }

  public Stage getStage() {
    return stage;
  }

  public void showDeathMessage(DamageSource source) {
    deathMessage.setSource(source);
    deathMessage.show(this.stage);
    healthBar.setPlayer(null);
  }

  public void reset() {
    if (map != null){
      map.dispose();
    }
    map = levels[currentLevel].getMap(this);
    healthBar.setPlayer(map.getPlayer());
    //this.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
  }

  public void showDialogue(DialogueDef dialogueDef) {
    Gdx.app.log("game","show dialogue:" + dialogueDef.name);
    radioChat.showDialogue(dialogueDef);
  }

  public void nextLevel() {
    currentLevel++;
    if (currentLevel > levels.length -1){
      currentLevel = 0;
    }
    reset();
  }

  public void hiddenPause() {
    map.pause();
  }

  public void visiblePause() {
    pauseMeesage.show(this.stage);
    map.pause();
  }

  public void  unpause(){
    pauseMeesage.hide();
    map.unpause();
  }

  public void showSuccessMessage() {
    successMessage.show(this.stage);
    hiddenPause();
  }

  public Map getMap() {
    return map;
  }
}
