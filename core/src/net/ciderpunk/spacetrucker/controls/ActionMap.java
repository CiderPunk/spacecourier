package net.ciderpunk.spacetrucker.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectMap;


/**
 * Created by matthewlander on 13/10/15.
 */
public class ActionMap implements InputProcessor {

	protected final ObjectMap<String, Action> actions;
	protected final IntMap<Array<Action>> bindings;

	public ActionMap(){
		actions = new ObjectMap<String, Action>();
		bindings = new IntMap<Array<Action>>();
	}

	public void registerAction(Action action){
    actions.put(action.key, action);
	}

	public void registerActions(Action[] actions){
		for (Action action : actions) {
			this.actions.put(action.key, action);
		}
	}

	protected Action getAction(String key){
		return actions.get(key,null);
	}


  public void loadDefaultConfig(){
    for (ObjectMap.Entry<String, Action> action : actions) {
      int code = action.value.defKeyCode;
      bind(code, action.value);
    }
  }

	public void loadConfig(String path){
		Preferences prefs = Gdx.app.getPreferences(path);
		for (ObjectMap.Entry<String, Action> action : actions) {
			int code = prefs.getInteger(action.key, action.value.defKeyCode);
			bind(code, action.value);
		}
	}

	public void saveConfig(String path){
		Preferences prefs = Gdx.app.getPreferences(path);
		for (IntMap.Entry<Array<Action>> binding : bindings) {
			for (Action action : binding.value) {
				prefs.putInteger(action.key, binding.key);
			}
		}
		prefs.flush();

	}

	public void unbind(int keyCode){
		bindings.remove(keyCode);
	}


	public void unbind (int code, Action action){
		Array<Action> actionlist = bindings.get(code);
		if (actionlist == null){
			actionlist = new Array<Action>();
			bindings.put(code, actionlist);
		}
	}
	public void bind(int code, Action action){
		if (action != null) {
			//get key binding
			Array<Action> actionlist = bindings.get(code);
			if (actionlist == null){
				actionlist = new Array<Action>();
				bindings.put(code, actionlist);
			}
			actionlist.add(action);
		}
	}

	@Override
	public boolean keyDown(int keyCode){
		//("input", "key pressed " + keyCode);
		Array<Action> actionlist = bindings.get(keyCode);
		boolean result = false;
		if (actionlist != null){
			for (Action action : actionlist) {
				result = action.keyDown() ? true : result;
			}
		}
		return result;
	}

	@Override
	public boolean keyUp(int keyCode){
		Array<Action> actionlist = bindings.get(keyCode);
		boolean result = false;
		if (actionlist != null){
			for (Action action : actionlist) {
				result = action.keyUp() ? true : result;
			}
		}
		return result;
	}

  @Override
  public boolean keyTyped(char character) {
    return false;
  }

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
