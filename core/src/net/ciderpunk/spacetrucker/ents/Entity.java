package net.ciderpunk.spacetrucker.ents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.ents.blufor.Player;
import net.ciderpunk.spacetrucker.map.Map;

/**
 * Created by Matthew on 10/01/2016.
 */
public abstract class Entity implements IEntity, IModelEntity {

  private static Vector3 temp = new Vector3();
  protected Body body;
  protected final Map owner ;
  protected ModelInstance modelInstance;
  protected float health;



  @Override
  public Vector2 getLoc(Vector2 loc) {
    return loc.set(body.getPosition());
  }

  public Entity(Map map, float x, float y, float angle, BodyDef.BodyType type){
    owner = map;
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = type;
    bodyDef.position.set(x,y);
    bodyDef.angle = angle;
    body = owner.getWorld().createBody(bodyDef);
  }


  public Entity(Map map, float x, float y, float angle){
    this(map,x,y,angle, BodyDef.BodyType.DynamicBody);
  }



  @Override
  public void doDraw(ModelBatch batch, Environment env) {
    batch.render(modelInstance, env);
  }

  //not happy with this... meh
  public Vector3 getPosition(){
    return modelInstance.transform.getTranslation(temp);
  }

  @Override
  public void cleanUp() {  }

  @Override
  public boolean update(float dT) {
    modelInstance.transform.setToTranslation(body.getPosition().x, body.getPosition().y, 0f);
    modelInstance.transform.rotate(Vector3.Z,  (float)Math.toDegrees(body.getAngle()));
    return true;
  }

  public void collision(Contact contact, ContactImpulse impulse, Object other) {
    if (other instanceof Player){
      ((Player)other).collision(contact, impulse, this);
    }

    //do something else....
  }

  public void hurt(float damage, DamageSource source){
    Gdx.app.log("Entity hurt", this.getClass().getName() + " hurt by " + source.name);
    if (health > 0) {
      health += damage;
      if (health <= 0) {
        this.kill(source);
      }
    }
  }


  static final Vector2 me = new Vector2();
  static final Vector2 him = new Vector2();

  public Vector2 vectorToPlayer(Vector2 vectToPlayer){
    return owner.getPlayer().getLoc(vectToPlayer).sub(getLoc(me));
  }


  protected abstract void kill(DamageSource source);

  public Body getBody() {
    return body;
  }
}
