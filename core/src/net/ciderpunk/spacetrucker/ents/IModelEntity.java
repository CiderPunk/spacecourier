package net.ciderpunk.spacetrucker.ents;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

/**
 * Created by Matthew on 12/01/2016.
 */
public interface IModelEntity {

  void doDraw(ModelBatch batch, Environment env);

}
