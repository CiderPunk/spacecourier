package net.ciderpunk.spacetrucker.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by Matthew on 12/01/2016.
 */
public interface IShapeEntity {
  void draw(ShapeRenderer shaper);
}
