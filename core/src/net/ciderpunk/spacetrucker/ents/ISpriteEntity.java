package net.ciderpunk.spacetrucker.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Matthew on 12/01/2016.
 */
public interface ISpriteEntity {
  void draw(SpriteBatch batch);
}
