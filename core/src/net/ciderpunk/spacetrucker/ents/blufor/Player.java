package net.ciderpunk.spacetrucker.ents.blufor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJoint;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;
import net.ciderpunk.spacetrucker.controls.Action;
import net.ciderpunk.spacetrucker.controls.IActionConsumer;
import net.ciderpunk.spacetrucker.ents.Entity;
import net.ciderpunk.spacetrucker.ents.IShapeEntity;
import net.ciderpunk.spacetrucker.ents.cargo.Cargo;
import net.ciderpunk.spacetrucker.ents.decoration.Explosion;
import net.ciderpunk.spacetrucker.map.Map;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;
import net.ciderpunk.spacetrucker.sound.SoundManager;

/**
 * Created by Matthew on 10/01/2016.
 */
public class Player extends Entity implements IActionConsumer, IShapeEntity {

  private final Environment playerEnv;

  private final Environment burnEnv;

  @Override
  public void draw(ShapeRenderer shaper) { //totally sub optimal buit i don't have time to figure out meshbuol;ding for the tetheer!
    if (this.tetheredCargo != null){
      //get locs
      this.getLoc(myLoc);
      tetheredCargo.getLoc(hisLoc);

      //draw tether...
      shaper.begin(ShapeRenderer.ShapeType.Line);
      shaper.setColor(Color.WHITE);
      shaper.line(myLoc, hisLoc);
      shaper.end();

    }
  }


  protected enum PlayerState{
    alive,
    exploding,
    dead
  }


  final Vector3 tempV3 = new Vector3();
  final Vector2 tempV2 = new Vector2();
  final Quaternion tempQ = new Quaternion();
  static Model courierModel;



  public final ModelInstance thrustModelInstance;
  public int actions = 0;

  public final PointLight pointLight;

	static final float thrustVolume = 0.2f;

  protected  DamageSource lastHit;

  static Sound sndThrustStart;
  static Sound sndThrustMid;
  static Sound sndThrustEnd;

  protected PlayerState state;

	protected long thrustMidId;
	protected long thrustStartId;
	protected long thrustEndId;
	protected float thrustTime = 0f;
  protected float ttl = 5f;

  protected Cargo tetheredCargo = null;


	private static final PlayerAction actionThrust = new PlayerAction("Thrust", "p1_thrust", 19, PlayerMove.thrust);
  private static final PlayerAction actionLeft = new PlayerAction("Turn left", "p1_left", 21, PlayerMove.left);
  private static final PlayerAction actionRight = new PlayerAction("Turn right", "p1_right", 22, PlayerMove.right);
  private static final PlayerAction actionShoot = new PlayerAction("Shoot", "p1_shoot", 62, PlayerMove.shoot);
  private static final PlayerAction actionTether = new PlayerAction("Tether", "p1_tether", 57, PlayerMove.Tether);

  @Override
  public boolean startAction(Action action) {
    if (state == PlayerState.alive) {
      actions = PlayerMove.addAction(actions, ((PlayerAction) action).move);
      //Gdx.app.log("player", "actions: " + actions);
      switch (((PlayerAction) action).move) {
        case thrust:
          stopSounds();
          thrustStartId = sndThrustStart.play(thrustVolume);
          thrustTime = 1f;
          break;

        case Tether:
          if (tetheredCargo != null){
            //drop cargo
            dropTether();
          }
          else{
            tryTether();
          }

          break;
      }
    }
    return true;
  }

  final Vector2 hisLoc = new Vector2();
  final Vector2 myLoc = new Vector2();


  Joint tetherJoint;

  private void tryTether() {
    //find nearest cargo
    Cargo target =  owner.findNearestCargo(this.getLoc(myLoc));
    if (target.getLoc(hisLoc).sub(myLoc).len2() < Constants.TetherRange2){
      //Gdx.app.log("player", "tethered " + target.getClass().getName() );
      //create tether!
      tetheredCargo = target;

      RopeJointDef jointDef = new RopeJointDef();
      jointDef.bodyA = this.body;
      jointDef.bodyB = target.getBody();
      jointDef.localAnchorA.set(Vector2.Zero);
      jointDef.localAnchorB.set(Vector2.Zero);
      jointDef.maxLength = Constants.TetherRange;

      tetherJoint = owner.getWorld().createJoint(jointDef);
    }
  }

  private void dropTether() {
    body.getWorld().destroyJoint(tetherJoint);
    tetheredCargo= null;
//tetherJoint.
  }

  private void drawTether(ModelBatch batch){
/*
    MeshBuilder mb = new MeshBuilder();
    mb.begin(VertexAttributes.Usage.Position);
    this.getLoc(myLoc);
    tetheredCargo.getLoc(hisLoc);
    MeshPart part = mb.part("tether", GL20.GL_TRIANGLES);
    mb.line(myLoc.x, myLoc.y, 0,hisLoc.x, hisLoc.y, 0);
    Mesh mesh = mb.end();
RenderablyProvider
batch.r
    batch.render(part);
    mesh.dispose();
    */
  }

  @Override
  public void doDraw(ModelBatch batch, Environment env) {
    batch.render(modelInstance, playerEnv);

    if (tetheredCargo!= null){
      drawTether(batch);
    }

    if (PlayerMove.isActive(PlayerMove.thrust, actions) || state == PlayerState.exploding) {
      //position thrust model...
      thrustModelInstance.transform.setToTranslation(body.getPosition().x, body.getPosition().y, 0f);
      thrustModelInstance.transform.rotate(Vector3.Z,  (float)Math.toDegrees(body.getAngle()));
      thrustModelInstance.transform.translate(0f,MathUtils.random(-0.5f,0.5f), 0f);
      batch.render(thrustModelInstance, this.burnEnv);
    }

  }

  @Override
  public boolean endAction(Action action) {

    if (state == PlayerState.alive) {
      actions = PlayerMove.subAction(actions, ((PlayerAction) action).move);
      switch (((PlayerAction) action).move) {
        case thrust:
          stopSounds();
          thrustMidId = 0;
          thrustEndId = sndThrustEnd.play(thrustVolume);
          //pointLight.setIntensity(500f);
          break;
      }
    }
    return true;
  }

  public float getHealth() {
    return health / Constants.PlayerMaxHealth;
  }

  //actions
  static{
    SpaceCourier.keymap.registerActions(new Action[]{ actionThrust, actionLeft, actionRight, actionShoot, actionTether });
  }


  public static class PlayerAction extends Action {
    public final PlayerMove move;
    public PlayerAction(String name, String key, int defKey, PlayerMove move) {
      super(name, key, defKey);
      this.move = move;
    }
  }

  float[] shapeVerts = {0f,4f, 1.8f,2.5f, 1.8f,-3.7f, -1.8f,-3.7f, -1.8f,2.5f};

  public Player(Map map, float x, float y, float angle) {
    super(map, x, y, angle);
    playerEnv = new Environment();
    playerEnv.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.3f, 0.2f, 0.2f, 1.0f));
    playerEnv.add(new DirectionalLight().set(0.4f, 0.2f, 0.2f, -1f, -0.8f, -0.2f));


    burnEnv = new Environment();
    burnEnv.set(new ColorAttribute(ColorAttribute.AmbientLight, 1f, 1f, 1f, 1.0f));


    state = PlayerState.alive;
    this.health = Constants.PlayerMaxHealth;
    PolygonShape shape = new PolygonShape();
    shape.set(shapeVerts);
    //shape.setAsBox(8f,12f);

    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.density = 0.5f;
    fixtureDef.filter.categoryBits = Constants.Player_Fixture;
    fixtureDef.filter.maskBits = Constants.Enemy_Fixture | Constants.World_Fixture | Constants.Bullet_Fixture | Constants.Sensor_Fixture | Constants.Cargo_Fixture;

    body.createFixture(fixtureDef);
    body.setAngularDamping(2f);
    //store this
    body.setUserData(this);

    shape.dispose();

    pointLight = new PointLight();
    pointLight.setIntensity(1000f).setColor(Color.WHITE);

    modelInstance   = new ModelInstance(courierModel, "ship");
    thrustModelInstance = new ModelInstance(courierModel, "thrust");

    actionThrust.addConsumer(this);
    actionLeft.addConsumer(this);
    actionRight.addConsumer(this);
    actionShoot.addConsumer(this);
    actionTether.addConsumer(this);
  }

  @Override
  public void cleanUp() {
    super.cleanUp();
    stopSounds();
    actionThrust.removeConsumer(this);
    actionLeft.removeConsumer(this);
    actionRight.removeConsumer(this);
    actionShoot.removeConsumer(this);
    actionTether.removeConsumer(this);
  }

  static final float shootLight = 3000f;
  static final float maxLight = 2000f;
  static final float minLight = 500f;
  static final float lightChangeRate = 100f;
  static final float shootDelay = 0.5f;

  float shootCooldown = 0f;
  float lightIntensity = minLight;

  protected final Vector2 position = new Vector2();
  protected final Vector2 velocity = new Vector2();


  protected void doThrust(float dT){
    tempV2.set(Vector2.Y ).scl(body.getInertia() * 10f);
    tempV2.rotateRad(body.getAngle());
    body.applyForceToCenter(tempV2, true);
    //start looping the mid thrust sound if we've boosted for over a second..
    thrustTime -= dT;
    if (thrustTime < 0 && thrustMidId == 0){
      thrustMidId = sndThrustMid.loop(thrustVolume);
    }
  }

  @Override
  public boolean update(float dT){
    super.update(dT);

    switch (state){
      case alive:
        if (PlayerMove.isActive(PlayerMove.thrust, actions)){
          doThrust(dT);
          if (lightIntensity < maxLight) {
            lightIntensity += lightChangeRate;
          }
        }
        else{
          if (lightIntensity > minLight) {
            lightIntensity -= lightChangeRate;
          }
        }
        if (PlayerMove.isActive(PlayerMove.left, actions)){
          body.applyAngularImpulse( body.getInertia() * 0.2f, true);
        }

        if (PlayerMove.isActive(PlayerMove.right, actions)){
          body.applyAngularImpulse(  body.getInertia() * -0.2f, true);
        }

        if (PlayerMove.isActive(PlayerMove.shoot, actions)){
          if (shootCooldown < 0f) {
            velocity.set(Vector2.Y).rotateRad(body.getAngle());
            position.set(body.getPosition()).mulAdd(velocity, 5f);
            velocity.scl(200f).add(body.getLinearVelocity());


            owner.addBullet(true, position, velocity, Constants.PlayerBulletDamage, DamageSource.Player);
            SoundManager.instance.shots[0].play(0.5f);
            lightIntensity = shootLight;
            //owner.addDecoration(Explosion.GetExplosion(this.getPosition(), 0.5f));
            shootCooldown = shootDelay;
          }
        }
        shootCooldown-= dT;
        break;
      case exploding:
        //outta control thrusting!

        doThrust(dT);
        lightIntensity = MathUtils.random(1000f,2000f);
        if (MathUtils.random(10)  == 0){
          owner.addDecoration(Explosion.GetExplosion(this.getPosition(), MathUtils.random(0.5f, 1f)));
        }
        ttl-=dT;
        if (ttl < 0){
          owner.addDecoration(Explosion.GetExplosion(this.getPosition(),1.5f));
          body.setActive(false);
          this.state = PlayerState.dead;
          stopSounds();
          return false;


        }
        break;
    }
    pointLight.setIntensity(lightIntensity);
    pointLight.setPosition(modelInstance.transform.getTranslation(tempV3));
    return true;
  }

  public void stopSounds() {
    //stop sounds
    sndThrustEnd.stop(thrustEndId);
    sndThrustStart.stop(thrustStartId);
    sndThrustMid.stop(thrustMidId);

    thrustEndId = thrustStartId= thrustMidId = 0;
  }


  public Vector2 getVelocity(Vector2 vel){
    return vel.set(body.getLinearVelocity());
  }

  protected final Vector2 normalImpulse = new Vector2();
  protected final Vector2 tangentImpulse = new Vector2();

  @Override
  public void collision(Contact contact, ContactImpulse impulse, Object other) {
    //world collision....
    if (other == null){
      float[] normal = impulse.getNormalImpulses();
      float[] tang = impulse.getTangentImpulses();
      normalImpulse.set(normal[0],normal[1]);
      tangentImpulse.set(tang[0], tang[1]);
      float totalImpulse = normalImpulse.add(tangentImpulse).len();
      //fixme: do some maths!
      float damage = (totalImpulse - 300f) / 4;

      if (totalImpulse >50){
        SoundManager.instance.scrapes[MathUtils.random(SoundManager.instance.scrapes.length-1)].play(0.5f);
      }
      if (totalImpulse > 200f){
        SoundManager.instance.hits[MathUtils.random(SoundManager.instance.hits.length-1)].play(0.5f);
      }
      if (damage > 0){
        hurt(-damage, DamageSource.Environment);
      }
    }
  }

  @Override
  protected void kill(DamageSource source) {

    //only do this once
    if (this.state == PlayerState.alive) {
      lastHit = source;
      owner.playerDead(source);
      this.state = PlayerState.exploding;
      this.ttl = Constants.PlayerDeathTime;
    }
  }



  public static class Loader implements  IResourceUser {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load("models/courier.g3dj", Model.class);
      resMan.load("sound/thruststart.wav", Sound.class);
      resMan.load("sound/thrustmid.wav", Sound.class);
	    resMan.load("sound/thrustend.wav", Sound.class);


    }

    @Override
    public void postLoad(ResourceManager resMan) {
      //model
      courierModel= resMan.get("models/courier.g3dj", Model.class);
      //sounds
      sndThrustStart = resMan.get("sound/thruststart.wav", Sound.class);
      sndThrustMid = resMan.get("sound/thrustmid.wav", Sound.class);
      sndThrustEnd = resMan.get("sound/thrustend.wav", Sound.class);

    }
  }


  ///enumeration of possible player keys
  protected enum PlayerMove{
    thrust(1),
    left(2),
    right(4),
    shoot(8),
    Tether(16);

    public final int mask;
    PlayerMove(int mask){
      this.mask = mask;
    }

    public static int addAction(int val, PlayerMove action){
      val = val | action.mask;
      return val;
    }

    public static int subAction(int val, PlayerMove action){
      val = val ^ action.mask;
      return val;
    }
    public static boolean isActive(PlayerMove move, int actions){
      return ((actions | move.mask)  == actions);
    }
  }


}
