package net.ciderpunk.spacetrucker.ents.cargo;

import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.ents.Entity;
import net.ciderpunk.spacetrucker.map.Map;

/**
 * Created by Matthew on 17/01/2016.
 */
public abstract class Cargo extends Entity {



  public Cargo(Map map, float x, float y, float angle) {
    super(map, x, y, angle);
  }

  @Override
  protected void kill(DamageSource source) {

  }





}
