package net.ciderpunk.spacetrucker.ents.cargo;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.map.Map;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;

/**
 * Created by Matthew on 17/01/2016.
 */
public class Crate extends Cargo {

  static Model crate;

  public Crate(Map map, float x, float y, float angle) {
    super(map, x, y, angle);
    modelInstance = new ModelInstance(crate);

    PolygonShape shape = new PolygonShape();
    shape.setAsBox( 4f,4f);

    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.density = 0.04f;
    fixtureDef.filter.categoryBits = Constants.Cargo_Fixture;
    fixtureDef.filter.maskBits = (short)(Constants.Enemy_Fixture | Constants.Player_Fixture | Constants.World_Fixture | Constants.Bullet_Fixture  | Constants.Cargo_Fixture |Constants.Sensor_Fixture);

    body.createFixture(fixtureDef);
    shape.dispose();
    //store this
    body.setUserData(this);
  }

  @Override
  public void hurt(float damage, DamageSource source) {
    //indestructable
  }


  @Override
  public boolean update(float dT) {
    super.update(dT);
    return true;
  }

  public static class Loader implements IResourceUser {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load("models/crate.g3dj", Model.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      crate = resMan.get("models/crate.g3dj", Model.class);

    }
  }
}
