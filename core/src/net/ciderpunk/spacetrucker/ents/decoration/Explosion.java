package net.ciderpunk.spacetrucker.ents.decoration;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.ents.IEntity;
import net.ciderpunk.spacetrucker.ents.ISpriteEntity;
import net.ciderpunk.spacetrucker.gfx.AnimationBuilder;
import net.ciderpunk.spacetrucker.gfx.Frame;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;
import net.ciderpunk.spacetrucker.sound.SoundManager;

/**
 * Created by Matthew on 14/01/2016.
 */
public class Explosion implements Pool.Poolable, IEntity, ISpriteEntity {


  static Animation explodeAnim;
  private static final Pool<Explosion> pool = new Pool<Explosion>(){
    @Override
    protected Explosion newObject() {
      return new Explosion();
    }
  };

  protected final Vector2 loc;
  protected Frame currentFrame;
  protected Animation currentAnim;
  protected float stateTime, scale, rot;

  public Explosion() {

    loc = new Vector2();
    // TODO Auto-generated constructor stub
  }

  protected void setAnim(Animation anim){
    stateTime = 0f;
    currentAnim = anim;
  }

  public static Explosion GetExplosion(Vector3 position, float scale){
    return pool.obtain().init(position.x, position.y,scale);
  }

  public static Explosion GetExplosion(Vector2 position, float scale){
    return pool.obtain().init(position.x, position.y,scale);
  }

  protected Explosion init(float x, float y, float scale){
    setAnim(explodeAnim);
    this.loc.set(x, y);
    this.rot = MathUtils.random(360);

    SoundManager.instance.explosions[MathUtils.random(3)].play(MathUtils.clamp(scale * 2f, 0.2f,1f));

    this.scale = 0.5f * scale;
    return this;
  }

  @Override
  public void cleanUp() {
    pool.free(this);
  }

  @Override
  public boolean update(float dT) {
    if (currentAnim != null){
      this.stateTime += dT;
      this.currentFrame = (Frame)currentAnim.getKeyFrame(this.stateTime, false);
      return !currentAnim.isAnimationFinished(this.stateTime);
    }
    return false;
  }

  @Override
  public Vector2 getLoc(Vector2 loc) {
    return loc.set(this.loc);
  }

  @Override
  public void draw(SpriteBatch batch) {
    currentFrame.draw(batch, loc.x, loc.y, rot, scale);
  }

  @Override
  public void reset() {

  }

  public static class Loader implements IResourceUser {

    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load(Constants.AtlasPath, TextureAtlas.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      explodeAnim =  AnimationBuilder.buildAnim(resMan.get(Constants.AtlasPath, TextureAtlas.class), "explode", 0.02f, 25, 25);
    }
  }
}
