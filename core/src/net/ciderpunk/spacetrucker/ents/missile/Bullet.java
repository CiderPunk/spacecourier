package net.ciderpunk.spacetrucker.ents.missile;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.ents.Entity;
import net.ciderpunk.spacetrucker.ents.IEntity;
import net.ciderpunk.spacetrucker.ents.ISpriteEntity;
import net.ciderpunk.spacetrucker.ents.decoration.Explosion;
import net.ciderpunk.spacetrucker.gfx.Frame;
import net.ciderpunk.spacetrucker.map.Map;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;

/**
 * Created by matt.lander on 15/01/2016.
 */
public class Bullet implements Pool.Poolable, IEntity, ISpriteEntity {

	static Frame bulletFrame;

  final Map owner;
  final Body body;
  final BulletPool pool;
  protected boolean alive;
  protected float ttl;
  private float damage;
  private DamageSource source;


  protected static float TimeToLive = 4f;

  protected Bullet(Map map, Shape shape, BulletPool pool, short maskBits){
    this.pool = pool;
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    bodyDef.awake = false;
    this.owner = map;

    body = map.getWorld().createBody(bodyDef);

    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.density = 0.1f;
    fixtureDef.filter.categoryBits = Constants.Bullet_Fixture;
    fixtureDef.filter.maskBits = maskBits;

    body.createFixture(fixtureDef);
    body.setUserData(this);
    body.setBullet(true);
    alive = false;
  }


  protected Bullet init(Vector2 pos, Vector2 velocity, float damage, DamageSource source) {
    this.damage = damage;
    body.setTransform(pos,0f);
    body.setLinearVelocity(velocity);
    body.setAwake(true);
    body.setActive(true);
    alive = true;
    ttl = TimeToLive;
    this.source = source;
    return this;
  }



  @Override
	public void cleanUp() {
    body.setAwake(false);
    body.setActive(false);
    pool.free(this);
    alive = false;
	}

	@Override
	public boolean update(float dT) {
    ttl -= dT;
    return alive && ttl > 0;
	}

  @Override
  public Vector2 getLoc(Vector2 loc) {
    return body.getWorldPoint(loc);
  }

  @Override
	public void draw(SpriteBatch batch) {
     batch.draw(bulletFrame, body.getPosition().x, body.getPosition().y, 1f, 1f);
	}

	@Override
	public void reset() {

	}

  public void hit(Contact contact, Object target) {

    if (target == null){
      //hit a wall

    }
    else{
      if (target instanceof Entity){

        ((Entity)target).hurt(damage, source);

      }

    }
    alive = false;
    owner.addDecoration(Explosion.GetExplosion(body.getPosition(), 0.1f));
  }

  public static class Loader implements IResourceUser {

		@Override
		public void preLoad(ResourceManager resMan) {
			resMan.load(Constants.AtlasPath, TextureAtlas.class);
		}

		@Override
		public void postLoad(ResourceManager resMan) {
			bulletFrame = new Frame(resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class).findRegion("bullet"),5,5);
		}
	}


}
