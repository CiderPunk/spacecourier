package net.ciderpunk.spacetrucker.ents.missile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.map.Map;

/**
 * Created by Matthew on 15/01/2016.
 */
public class BulletPool extends Pool<Bullet> implements Disposable {
  final CircleShape circle;
  final Map map;

  final short mask;
  public BulletPool(Map map,  short maskBits){
    super(20);
    this.map = map;
    circle = new CircleShape();
    circle.setRadius(0.5f);
    this.mask = maskBits;
  }

  @Override
  protected Bullet newObject() {
    return new Bullet(map, circle, this, mask);
  }

  public Bullet getBullet(Vector2 pos, Vector2 velocity, float damage, DamageSource source){
    return this.obtain().init(pos, velocity, damage, source);
  }

  @Override
  public void dispose() {
    circle.dispose();
  }
}