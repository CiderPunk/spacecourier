package net.ciderpunk.spacetrucker.ents.opfor;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.ents.Entity;
import net.ciderpunk.spacetrucker.ents.decoration.Explosion;
import net.ciderpunk.spacetrucker.map.Map;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;

/**
 * Created by Matthew on 17/01/2016.
 */
public class GunBot extends Entity {


  static final float StartHealth = 0.6f;
  static final float FireRate = 0.6f;
  static final float FireVariance = 0.4f;

  static Model gunModel;
  boolean alive = true;
  float ttf = 0f;



  float[] shapeVerts = {-4.5f,8f,4.5f,8f,9f,-8f,-9f,-8f};

  private final Vector2 fireOffset;

  public GunBot(Map map, float x, float y, float angle) {
    super(map, x, y, angle, BodyDef.BodyType.StaticBody);
    modelInstance = new ModelInstance(gunModel);

    PolygonShape shape = new PolygonShape();
    shape.set(shapeVerts);

    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.density = 0.04f;
    fixtureDef.filter.categoryBits = Constants.Enemy_Fixture;
    fixtureDef.filter.maskBits = (short)( Constants.Player_Fixture | Constants.Bullet_Fixture  | Constants.Cargo_Fixture );

    body.createFixture(fixtureDef);
    shape.dispose();
    //store this
    body.setUserData(this);
    health = StartHealth;
    ttf = FireRate + MathUtils.random(FireVariance);

    angle = body.getAngle();

    fireOffset =new Vector2(Vector2.Y).scl(8f).rotateRad(angle);
    fireOffset.add(this.getLoc(loc));

  }




  @Override
  protected void kill(DamageSource source) {
    alive = false;
    owner.addDecoration(Explosion.GetExplosion(body.getPosition(), 1f));
  }

  static final Vector2 loc = new Vector2();
  static final Vector2 dir = new Vector2();
  static final Vector2 targetVector = new Vector2();
  static final float PIhalf = MathUtils.PI / 2;


  static final float ActivationRange2 = 200 * 200;

  @Override
  public boolean update(float dT) {
    super.update(dT);
    if (!alive){
      body.setActive(false);
      return false;
    }

    ttf -= dT;

    vectorToPlayer(targetVector);
    if (targetVector.len2() < ActivationRange2) {
      if (ttf < 0) {
        float angle = body.getAngle();
        angle += MathUtils.random(MathUtils.PI) - PIhalf;
        dir.set(0, 200f).rotateRad(angle);
        //fire!
        owner.addBullet(false, fireOffset, dir, -20f, DamageSource.GunBot);
        ttf = FireRate + MathUtils.random(FireVariance);
      }
    }
    return true;
   }

  public static class Loader implements IResourceUser {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load("models/gun.g3dj", Model.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      gunModel = resMan.get("models/gun.g3dj", Model.class);
    }
  }


}
