package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.sun.deploy.uitoolkit.impl.fx.ui.FXMessageDialog;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;
import net.ciderpunk.spacetrucker.controls.Action;
import net.ciderpunk.spacetrucker.controls.IActionConsumer;

/**
 * Created by Matthew on 15/01/2016.
 */
public class DeathMessage extends MessageDialogue {
  DamageSource source;

  public DeathMessage(Skin skin, SpaceCourier game) {
    super(skin, game, "Game Over","Restart");
  }

  @Override
  boolean done() {
    game.reset();
    return true;
  }

  @Override
  String getContent() {
    return "Killed by " + source.getMessage();
  }

  public void setSource(DamageSource source) {
    this.source = source;
  }
}
