package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import net.ciderpunk.spacetrucker.ents.blufor.Player;

/**
 * Created by Matthew on 19/04/2015.
 */
public class HealthBar extends Actor {




  final Skin skin;
  NinePatchDrawable frame;
  NinePatchDrawable inner;
  Player player;

  public HealthBar(Skin skin) {
    super();
    this.skin = skin;
    this.setWidth(200);
    this.setHeight(30);
    this.setTouchable(Touchable.disabled);
    Color tint = skin.getColor("fade");
    frame = new NinePatchDrawable(skin.getPatch("gui/healthframe")).tint(tint);
    inner = new NinePatchDrawable(skin.getPatch("gui/healthbar")).tint(tint);

  }

  public void setPlayer(Player player){
    this.player = player;
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    if (this.player != null) {
      Color col = getColor();
      batch.setColor(col.r, col.g, col.b, col.a * parentAlpha);
      frame.draw(batch, getX(), getY(), getWidth(), getHeight());
      if (player.getHealth() > 0) {
        inner.draw(batch, getX() + 3, getY() + 3, player.getHealth() * (getWidth() - 6), getHeight() - 6);
      }
      batch.setColor(col.r, col.g, col.b, 1f);
    }
  }
}
