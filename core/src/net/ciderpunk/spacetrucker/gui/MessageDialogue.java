package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;

/**
 * Created by Matthew on 17/01/2016.
 */
public abstract class MessageDialogue extends Dialog {


  final protected MessageDialogue self;
  Label contentLabel;
  DamageSource source;
  final TextButton btn;
  final SpaceCourier game;

  public MessageDialogue(Skin skin, SpaceCourier game, String title, String buttonText) {
    super(title, skin, "dialog");
    this.game = game;
    self = this;
    this.pad(30, 10, 10, 10);
    contentLabel = new Label("", skin);
    this.getContentTable().add(contentLabel);
    btn = new TextButton(buttonText, skin);
    btn.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        self.hide();
        self.done();
      }

      @Override
      public boolean keyDown(InputEvent event, int keycode) {
        switch (keycode){
          case 66:
          case 62:
            self.hide();
            return done();
        }
        return false;
      }
    });
    this.getButtonTable().add(btn);
  }

  abstract boolean done();

  abstract String getContent();

  @Override
  public Dialog show(Stage stage) {
    contentLabel.setText( getContent());
    this.getContentTable().pack();
    this.pack();

    super.show(stage);
    stage.setKeyboardFocus(btn);
    return this;
  }

}
