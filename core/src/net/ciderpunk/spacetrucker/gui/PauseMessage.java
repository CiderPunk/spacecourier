package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;

/**
 * Created by Matthew on 15/01/2016.
 */
public class PauseMessage extends MessageDialogue {

  DamageSource source;

  public PauseMessage(Skin skin, SpaceCourier game) {
    super(skin, game, "Paused","Continue");
  }

  @Override
  boolean done() {

    game.unpause();
    return true;
  }


  static final String[] reasons = new String[]{"read a book?", "have a cup of tea?", "dance the fandango?", "contemplate the futility of existence?"};
  @Override
  String getContent() {
    return "Game paused, why not " + reasons[MathUtils.random(reasons.length-1)];
  }

}