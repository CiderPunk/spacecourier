package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Scaling;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.gfx.AnimationBuilder;
import net.ciderpunk.spacetrucker.level.DialogueDef;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;
import net.ciderpunk.spacetrucker.sound.SoundManager;

/**
 * Created by Matthew on 16/01/2016.
 */
public class RadioChat extends Table{

  static ObjectMap<String, Animation> avatars = new ObjectMap<String, Animation>();


  float ttl;
  DialogueDef def = null;
  Animation currentAnim;
  float stateTime = 0f;
  Stage stage;
  final Label content;
  final Image avatarImage;
  long soundId;
  Sound currentSound = null;


  TextureRegionDrawable textureRegionDrawable = new TextureRegionDrawable();

  public RadioChat( Skin skin){
    super(skin);
    avatarImage = new Image();
    content = new Label("", skin);
    this.row().height(128f);
    avatarImage.setScaling(Scaling.fit);
    avatarImage.setDrawable(textureRegionDrawable);
    this.add(avatarImage).left().bottom().width(128f).height(150f);
    this.add(content).right().bottom();
    this.setVisible(false);
  }

  public void showDialogue(DialogueDef def){
    if (currentSound != null) {
      currentSound.stop(soundId);
    }
    this.setVisible(true);
    this.def = def;
    ttl = def.time;
    content.setText(def.text);
    stateTime = 0f;
    currentAnim = avatars.get(def.avatar);
    Sound[] voices = SoundManager.instance.voices.get(def.avatar);

    currentSound = voices[MathUtils.random(voices.length-1)];
    soundId = currentSound.loop(0.4f);
    textureRegionDrawable.setRegion(currentAnim.getKeyFrame(this.stateTime, true));
    this.pack();
  }

  @Override
  public void act(float delta) {
    super.act(delta);
    if (def==null){
      return;
    }
    if ((ttl-=delta) < 0){
      if (this.def.nextLine != null){
        showDialogue(this.def.nextLine);
      }
      else {
        //stop sound
        if (currentSound != null) {
          currentSound.stop(soundId);
          currentSound = null;
        }
        this.setVisible(false);
      }
    }
    else {
      stateTime+=delta;
      textureRegionDrawable.setRegion(currentAnim.getKeyFrame(this.stateTime, true));
    }
  }


  public static class Loader implements IResourceUser{
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load("res.atlas", TextureAtlas.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      TextureAtlas atlas=  resMan.get("res.atlas", TextureAtlas.class);
      avatars.put("lizard", AnimationBuilder.buildAnim(atlas, "radio/lizard", 0.2f, 0,0));
      avatars.put("shroom", AnimationBuilder.buildAnim(atlas, "radio/shroom", 0.2f, 0,0));
      avatars.put("nose", AnimationBuilder.buildAnim(atlas, "radio/nose", 0.2f, 0,0));
      avatars.put("sparks", AnimationBuilder.buildAnim(atlas, "radio/sparks", 0.2f, 0,0));
      avatars.put("brain", AnimationBuilder.buildAnim(atlas, "radio/brain", 0.2f, 0,0));

      avatars.put("explode", AnimationBuilder.buildAnim(atlas, "explode", 0.02f, 0, 0));
    }
  }
}
