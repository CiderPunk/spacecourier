package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;

/**
 * Created by Matthew on 15/01/2016.
 */
public class SuccessMessage  extends MessageDialogue {

  public SuccessMessage(Skin skin, SpaceCourier game) {
    super(skin, game, "Mission success","Continue");
  }

  @Override
  boolean done() {
    game.nextLevel();
    return true;
  }

  @Override
  String getContent() {
    return "Congratulations, you completed the level\n\ntime: " + String.format("%.2f", game.getMap().getTime());
  }

}