package net.ciderpunk.spacetrucker.gui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import net.ciderpunk.spacetrucker.SpaceCourier;

import java.sql.Time;

/**
 * Created by Matthew on 17/01/2016.
 */
public class Timer extends Label {

  final SpaceCourier game;

  public Timer(SpaceCourier game, Skin skin){
    super("0123456789", skin, "score-font", skin.getColor("score-colour"));
    this.game = game;
  }

  @Override
  public void act(float delta) {
    this.setText(String.format("%.2f",game.getMap().getTime()));
    super.act(delta);
  }

}

