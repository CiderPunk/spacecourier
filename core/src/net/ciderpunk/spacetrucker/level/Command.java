package net.ciderpunk.spacetrucker.level;

import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by matthewlander on 29/02/2016.
 */
public enum Command {
	Repeat("rpt"),
	Dialogue("dlg"),
	End("end"),
	OutOfBounds("oob"),
	PlayerCondition("cply"),
	CargoCondition("ccrg"),
	Set("set"),
	Test("test"),
	Rotate("rot"),
	FollwPath("fop"),
	Unknown("");



	final String code;
	Command(String code){
		this.code = code;
	}

	private static ObjectMap<String, Command> commandMap;
	private static ObjectMap<String, Command> getCommandMap(){
		if (commandMap == null) {
			commandMap = new ObjectMap<String, Command>(Command.values().length);
			for (Command com: Command.values()) {
				commandMap.put(com.code, com);
			}
		}
		return commandMap;
	}

	public static Command getCode(String val){
		return getCommandMap().get(val, Command.Unknown);
	}



}
