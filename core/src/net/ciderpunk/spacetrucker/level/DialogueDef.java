package net.ciderpunk.spacetrucker.level;

/**
 * Created by Matthew on 16/01/2016.
 */
public class DialogueDef   {
  public String name;
  public String text;
  public String avatar;
  public float time;
  public String next;

  public DialogueDef nextLine;

}
