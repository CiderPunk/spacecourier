package net.ciderpunk.spacetrucker.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectMap;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;
import net.ciderpunk.spacetrucker.ents.blufor.Player;
import net.ciderpunk.spacetrucker.ents.cargo.Cargo;
import net.ciderpunk.spacetrucker.map.Map;

/**
 * Created by Matthew on 16/01/2016.
 */
public class Level {

  public final String mapFile, dlgFile;
  SpaceCourier game;
  ObjectMap<String, DialogueDef> dialogues;

  final Array<String> keylist = new Array<String>();

  public Level(String mapFile, String dlgFile){
    this.mapFile = mapFile;
    this.dlgFile = dlgFile;
  }

  public Map getMap(SpaceCourier game){
    this.game = game;
    //load dialogue content
    readDialogue();
    //reset keylist
    keylist.clear();
    return new Map(game, this);
  }

  public boolean event(String[] commands, Object target){
    boolean done = true;
    int pos = 0;
    while (pos < commands.length) {
      switch(Command.getCode(commands[pos++])){
        //tests
        case CargoCondition:
          if (!(target instanceof Cargo)){
            return false;
          }
          break;
        case PlayerCondition:
          if (!(target instanceof Player)){
            return false;
          }
          break;
        case Test:
          if (!keylist.contains(commands[pos++], false)){
            return false;
          }
          break;
        case Dialogue:
          game.showDialogue(dialogues.get(commands[pos++]));
          break;
        case OutOfBounds:
          game.showDeathMessage(DamageSource.RunAway);
          break;
        case Repeat:
          done = false;//do nothing
          break;
        case Set:
          keylist.add(commands[pos++]);
          break;
        case End:
          this.endLevel();
          break;
      }
    }
    return done;

  }

  protected void endLevel(){
    //ovveride to add some conditional checks
    game.showSuccessMessage();

  }

  protected void readDialogue(){
    FileHandle fh = Gdx.files.internal(dlgFile);
    Json json = new Json();
    json.setElementType(DialogueCollection.class, "dialogues", DialogueDef.class);
    DialogueCollection dlgCollection = json.fromJson(DialogueCollection.class, fh);
    dialogues = new ObjectMap<String, DialogueDef>(dlgCollection.dialogues.size());
    //create object map
    for (DialogueDef dialogue : dlgCollection.dialogues) {
      dialogues.put(dialogue.name, dialogue);
    }

    //set up linked dialogues
    for (DialogueDef dialogue : dlgCollection.dialogues) {
      if (dialogue.next != null && dialogue.next != ""){
        dialogue.nextLine = dialogues.get(dialogue.next);
      }
    }
  }

/*
  public enum LevelEventType{
    Repeat("rpt"),
    Dialogue("dlg"),
    End("end"),
    OutOfBounds("oob"),
    PlayerCondition("cply"),
    CargoCondition("ccrg"),
    Set("set"),
    Test("test"),
    Unknown("");

    final String code;
    LevelEventType(String code){
      this.code = code;
    }
    public static LevelEventType getCode(String val){
      if (val!= null){
        for (LevelEventType le : LevelEventType.values()) {
          if (val.equalsIgnoreCase(le.code)) {
            return le;
          }
        }
      }
      return LevelEventType.Unknown;
    }


  }
  */
}
