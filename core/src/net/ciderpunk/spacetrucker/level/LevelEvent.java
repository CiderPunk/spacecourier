package net.ciderpunk.spacetrucker.level;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;

/**
 * Created by Matthew on 16/01/2016.
 */
public class LevelEvent {

  private final Level level;
  private final String[] commands;
  private boolean repeat;
  private boolean done = false;

  public LevelEvent(Level level, String commands){
   this.level = level;
    this.commands = commands.split(",");
  }

  public void trigger(Object target){
    if (!done) {
      done = level.event(commands, target);
    }

  }

}
