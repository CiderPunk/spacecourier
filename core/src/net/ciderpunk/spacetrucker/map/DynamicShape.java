package net.ciderpunk.spacetrucker.map;

import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.ents.blufor.Player;
import net.ciderpunk.spacetrucker.ents.cargo.Cargo;
import net.ciderpunk.spacetrucker.level.Command;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.shapes.Shape;

/**
 * Created by matthewlander on 29/02/2016.
 */
public class DynamicShape {

	final Geometry shape;
	float rotation = 0f;


	public DynamicShape(Geometry shape){
		this.shape = shape;
/*
		if (shape instanceof Group){
			((Group)shape).weld();
		}
*/
		if (shape.desc != null) {
			String[] commands = shape.desc.split(",");
			boolean done = true;
			int pos = 0;
			while (pos < commands.length) {
				switch (Command.getCode(commands[pos++])) {
					case Rotate:
						rotation = Float.parseFloat(commands[pos++]);
						shape.getBody().setAngularVelocity(rotation);
						//shape.getBody().setAngularDamping(0f);
						break;
				}
			}
		}
	}

	public void update(){
		shape.update();
	}
}
