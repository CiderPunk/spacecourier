package net.ciderpunk.spacetrucker.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import net.ciderpunk.spacetrucker.Constants;
import net.ciderpunk.spacetrucker.DamageSource;
import net.ciderpunk.spacetrucker.SpaceCourier;
import net.ciderpunk.spacetrucker.controls.Action;
import net.ciderpunk.spacetrucker.controls.IActionConsumer;
import net.ciderpunk.spacetrucker.ents.Entity;
import net.ciderpunk.spacetrucker.ents.EntityList;
import net.ciderpunk.spacetrucker.ents.IEntity;
import net.ciderpunk.spacetrucker.ents.blufor.Player;
import net.ciderpunk.spacetrucker.ents.cargo.Cargo;
import net.ciderpunk.spacetrucker.ents.cargo.Crate;
import net.ciderpunk.spacetrucker.ents.missile.Bullet;
import net.ciderpunk.spacetrucker.ents.missile.BulletPool;
import net.ciderpunk.spacetrucker.ents.opfor.GunBot;
import net.ciderpunk.spacetrucker.level.Level;
import net.ciderpunk.spacetrucker.level.LevelEvent;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.shapes.*;
import net.ciderpunk.svglib.geometry.shapes.Shape;

/**
 * Created by matthewlander on 28/12/2015.
 */
public class Map implements Disposable, IActionConsumer {


  //controls
  private static final MapAction actionToggleDebug = new MapAction("Toggle debug", "toggleDebug", 244, MapActionType.ToggleDebug);
  private static final MapAction actionPause = new MapAction("Pause", "pause", 131, MapActionType.Pause);
	private float time;

	SpaceCourier game;
	World world;

	@Override
  public boolean startAction(Action action) {
    switch(((MapAction)action).action){
      case ToggleDebug:
        showDebug = !showDebug;
        break;
      case Pause:
        game.visiblePause();
        break;
    }
    return true;
  }

  @Override
  public boolean endAction(Action action) {
    return true;
  }

  public void playerDead(DamageSource source) {
    this.game.showDeathMessage(source);
  }

  public Cargo findNearestCargo(Vector2 position) {
    IEntity ent = cargo.FindNearest(position);
    if (ent instanceof  Cargo){
      return (Cargo) ent;
    }
    return null;
  }


  protected boolean paused = false;
  public void pause() {
    paused = true;
  }
  public void unpause() {
    paused = false;
  }

  public float getTime() {
    return time;
  }


  protected enum MapActionType{
    ToggleDebug,
    Pause,
  }

  public static class MapAction extends Action {
    public final MapActionType action;
    public MapAction(String name, String key, int defKey, MapActionType action) {
      super(name, key, defKey);
      this.action = action;
    }
  }
  //actions
  static{
    SpaceCourier.keymap.registerActions(new Action[]{ actionToggleDebug, actionPause});
  }

  private final Camera cam;
  private final ModelBatch modelBatch;
  private final SpriteBatch spriteBatch;
  private final ShapeRenderer shapeRenderer;

  private static Box2DDebugRenderer debugRenderer;
  private SvgDoc doc;

  private final EntityList bluFor;
  private final EntityList decorations;
  private final EntityList missiles;
  private final EntityList cargo;
  private final EntityList opFor;


	final Array<DynamicShape> dynamicShapes = new Array();

	final Array<Group> backGroups = new Array<Group>();
	final Array<Group> foreGroups = new Array<Group>();

	public final BulletPool bluBulletPool;
  public final BulletPool opForBulletPool;
  private final Environment env = new Environment();
  protected Player player;
  private boolean showDebug;
  float accumulator = 0f;


  public World getWorld() {
    return world;
  }

  public void createTestBody(Vector2 pos) {
    BodyDef bodyDef = new BodyDef();
    bodyDef.type  = BodyDef.BodyType.DynamicBody;
    bodyDef.position.set(pos);
    Body body = world.createBody(bodyDef);
    CircleShape circle = new CircleShape();
    circle.setRadius(5f);
    body.createFixture(circle, 01f);
    circle.dispose();
  }


  final Level level;

	public Map(SpaceCourier game, Level level){
    actionPause.addConsumer(this);
    actionToggleDebug.addConsumer(this);
    //create box2d world
    world = new World(new Vector2(0f,-20f), true);

    bluBulletPool = new BulletPool(this, (short) (Constants.World_Fixture | Constants.Player_Fixture | Constants.Enemy_Fixture) );
    opForBulletPool = new BulletPool(this, (short) (Constants.World_Fixture | Constants.Player_Fixture) );
    this.level = level;
    time = 0f;

    world.setContactListener(new ContactListener() {
      @Override
      public void beginContact(Contact contact) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        if (obj1 instanceof Bullet){
          ((Bullet)obj1).hit(contact, obj2);
        }
        if (obj2 instanceof Bullet){
          ((Bullet)obj2).hit(contact, obj1);
        }

        //sensor events
        if (obj1 instanceof LevelEvent){
          ((LevelEvent) obj1).trigger(obj2);
        }
        if (obj2 instanceof LevelEvent){
          ((LevelEvent) obj2).trigger(obj1);
        }
      }
      @Override
      public void endContact(Contact contact) {
      }
      @Override
      public void preSolve(Contact contact, Manifold oldManifold) {
      }
      @Override
      public void postSolve(Contact contact, ContactImpulse impulse) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        if (obj1 instanceof Entity){
          ((Entity)obj1).collision(contact, impulse, obj2);
        }
        if (obj2 instanceof Entity){
          ((Entity)obj2).collision(contact, impulse, obj1);
        }
      }
    });

    cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    cam.position.set(0f,0f,200f);
    cam.lookAt(0,0,0);
    cam.near = 1f;
    cam.far = 2000f;
    cam.update();

    modelBatch = new ModelBatch();
    spriteBatch = new SpriteBatch();
    shapeRenderer = new ShapeRenderer();
    debugRenderer = new Box2DDebugRenderer();

    bluFor = new EntityList();
    decorations = new EntityList();
    missiles = new EntityList();
    cargo = new EntityList();
    opFor = new EntityList();

    env.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.3f, 0.3f, 1.0f));
    //env.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

    this.game = game;


    this.doc = new SvgDoc(level.mapFile);

    for(Group group : doc.root.getGroups()){
      switch(GroupType.fromString(group.id)){
        case Ents:
          //parse entities
          parseEntities(group);
          break;
        case Fore: {
          ModelDef def = new ModelDef(-20, 40);
          group.buildModel(def);
					foreGroups.add(group);
          break;
        }
        case Back: {
          ModelDef def = new ModelDef(-20, -30);
          group.buildModel(def);
					backGroups.add(group);
          break;
        }
        case Deep: {
          ModelDef def = new ModelDef(-100, 50);
          group.buildModel(def);
        	addToWorldStatic(group);
					backGroups.add(group);
          break;
        }
        case Sensor:
					addToWorldSensor(group);
          break;
				case Dynamic: {
					addDynamicShapes(group);
					ModelDef def = new ModelDef(-60, 30);
					group.buildModel(def);

					backGroups.add(group);
					break;
				}
        default: {
					ModelDef def = new ModelDef(-60, 30);
					group.buildModel(def);
					addToWorldStatic(group);
					backGroups.add(group);
					break;
				}
      }
    }
	}

	private void addDynamicShapes(Group group) {

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.filter.categoryBits = Constants.World_Fixture;
		fixtureDef.filter.maskBits = (short) (Constants.Player_Fixture | Constants.Cargo_Fixture);
		BodyDef bodyDef = new BodyDef();
		bodyDef.type= BodyDef.BodyType.KinematicBody;
		for (Geometry child : group.getChildren()) {
			child.addToWorld(world,bodyDef, fixtureDef, true);
			dynamicShapes.add(new DynamicShape(child));
		}

	}


	private void parseEntities(Group group) {
		for (Geometry shape : group.getChildren()) {
			if(shape instanceof Group){
				parseEntities((Group)shape);
			}
			if (shape instanceof Text){
				Text text = (Text) shape;
				switch (EntityType.fromString(text.text)){
					case Player:
						this.player = new Player(this, text.transformedPosition.x, text.transformedPosition.y, text.rotation);
						bluFor.add(this.player);
						env.add(player.pointLight);
						currentLook.set(player.getPosition().x, player.getPosition().y);
						break;

					case Crate:
						Crate crate = new Crate(this, text.transformedPosition.x, text.transformedPosition.y,text.rotation);
						cargo.add(crate);
						break;

					case Gun:
						GunBot gunBot = new GunBot(this, text.transformedPosition.x, text.transformedPosition.y,text.rotation);
						opFor.add(gunBot);
						break;

				}
			}
		}
  }

	private void addToWorldSensor(Group group) {
		//create sensors that dont interract physically with the player but trigger events
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Constants.Sensor_Fixture;
		fixtureDef.filter.maskBits = (short) (Constants.Player_Fixture | Constants.Cargo_Fixture);
		BodyDef bodyDef = new BodyDef();
		bodyDef.type= BodyDef.BodyType.StaticBody;
		group.addToWorld(world, bodyDef, fixtureDef, false );
		Array<Shape> sensors = new Array<Shape>();
		group.getShapesRecursive(sensors);
		for (Shape sensor : sensors) {
			sensor.getBody().setUserData(new LevelEvent(level, sensor.desc));
		}

	}


	private void addToWorldStatic(Group group){
    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.density = 1f;
		fixtureDef.restitution = 0.2f;
		BodyDef bodyDef = new BodyDef();
    bodyDef.type= BodyDef.BodyType.StaticBody;
    group.addToWorld(world, bodyDef, fixtureDef, false );
  }


  public void addBullet(boolean player, Vector2 position, Vector2 velocity, float damage, DamageSource source){
    missiles.add(
      player ?
              bluBulletPool.getBullet(position, velocity, damage, source) :
              opForBulletPool.getBullet(position, velocity, damage, source)
    );
  }



	public void draw() {
    cam.update();
    modelBatch.begin(cam);
		for (Group backGroup : backGroups) {
			backGroup.draw(modelBatch, env);
		}
    modelBatch.end();

    shapeRenderer.setProjectionMatrix(cam.combined);
    bluFor.doDraw(shapeRenderer);

    modelBatch.begin(cam);

    opFor.doDraw(modelBatch, env);
    bluFor.doDraw(modelBatch, env);
    cargo.doDraw(modelBatch, env);

    modelBatch.end();

    spriteBatch.setProjectionMatrix(cam.combined);
    spriteBatch.begin();

    missiles.doDraw(spriteBatch);
    decorations.doDraw(spriteBatch);

    spriteBatch.end();


    modelBatch.begin(cam);
		for (Group group : foreGroups) {
			group.draw(modelBatch, env);
		}
    modelBatch.end();

    if (showDebug){
      debugRenderer.render(world,cam.combined);
    }
	}

  public void addDecoration(IEntity ent){
    decorations.add(ent);
  }

  protected final Vector2 velocity = new Vector2();
  protected final Vector2 currentVelocity = new Vector2();
  protected final Vector2 currentPosition = new Vector2();
  protected final Vector2 change = new Vector2();
  protected final Vector2 currentLook = new Vector2();
  float currentDist = 300f;

  public void update(){
    float dT = Gdx.graphics.getDeltaTime();
    if (dT > 0.5f || paused){
      //no point catching up over half a second, skip this one
      return;
    }

    time += dT;

    accumulator +=dT;
    while(accumulator >= Constants.SimulationSpeed ) {
      accumulator -= Constants.SimulationSpeed;
      world.step(Constants.SimulationSpeed, 6, 2);
      bluFor.doUpdate(Constants.SimulationSpeed);
      cargo.doUpdate(Constants.SimulationSpeed);
      opFor.doUpdate(Constants.SimulationSpeed);


			for (DynamicShape dynamicShape : dynamicShapes) {
				dynamicShape.update();
			}

    }

    decorations.doUpdate(dT);
    missiles.doUpdate(dT);

    //store prev position
    change.set(currentPosition);
    //get current position
    currentPosition.set(player.getPosition().x, player.getPosition().y);
    //calc change
    change.sub(currentPosition);
    currentLook.sub(change);

    //get smoothed velocity
    currentVelocity.lerp(player.getVelocity(velocity), 0.05f);
    float vel = currentVelocity.len();
    //calc dist based on smoothed speed
    float dist = 100f +  ((vel * vel / 14400f) * 150f);
    currentLook.set(currentPosition).mulAdd(currentVelocity, 0.8f);

//		cam.up.set(0,0,-1);

		//update cam
	//	cam.up.set(0,0,-1);
    cam.position.set(currentPosition.x, currentPosition.y, dist);
		cam.lookAt(currentLook.x, currentLook.y, 0f);
		cam.update();
		//trippy!
		//cam.up.set(0,0,-1);
  }

	@Override
	public void dispose() {
    //clean out ents
    bluFor.clear();
    cargo.clear();
		doc.dispose();
		player.stopSounds();
    actionPause.removeConsumer(this);
    actionToggleDebug.removeConsumer(this);

    world.dispose();
    modelBatch.dispose();
    spriteBatch.dispose();
    shapeRenderer.dispose();
	}

  public Player getPlayer(){
    return  this.player;
  }

  protected enum EntityType{
    Player("p", "player"),
    Crate("c","crate"),
    Gun("g", "gun"),
    Unknown("un","unknown");
    //more to come!

    private String shortName, longName;
    EntityType(String shortName, String longName){
      this.shortName = shortName; this.longName = longName;
    }

    public static EntityType fromString(String val){
      if (val != null){
        for (EntityType et : EntityType.values()) {
          if (val.equalsIgnoreCase(et.shortName) || val.equalsIgnoreCase(et.longName)) {
            return et;
          }
        }
      }
      return null;
    }
  }

  protected enum GroupType{
    Ents("ents"),
    World("world"),
    Back("back"),
    Other("other"),
    Fore("fore"),
    Deep("deep"),
		Dynamic("dynamic"),
    Sensor("sensor");

    private String text;
    GroupType(String text){ this.text = text; }
    public static GroupType fromString(String val){
      if (val != null){
        for (GroupType gt : GroupType.values()) {
          if (val.equalsIgnoreCase(gt.text)) {
            return gt;
          }
        }
      }
      return Other;
    }
  }


}
