package net.ciderpunk.spacetrucker.resources;
public interface IResourceUser {
	void preLoad(ResourceManager resMan);
	void postLoad(ResourceManager resMan);
}
