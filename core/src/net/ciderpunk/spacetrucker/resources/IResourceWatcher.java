package net.ciderpunk.spacetrucker.resources;

/**
 * Created by Matthew on 09/01/2016.
 */
public interface IResourceWatcher extends IResourceUser {
  void loadComplete();
}
