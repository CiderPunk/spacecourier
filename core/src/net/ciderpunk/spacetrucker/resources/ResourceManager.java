package net.ciderpunk.spacetrucker.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.PooledLinkedList;

public class ResourceManager implements Disposable {

	AssetManager assetMan;
	PooledLinkedList<IResourceUser> postLoadList;
	boolean loading;
	IResourceWatcher owner;

	public AssetManager getAssetMan() {
		return assetMan;
	}

  public void load(String filename, Class type){
    assetMan.load(filename, type);
  }

  public <T> T get(String filename, Class<T> type){
    return assetMan.get(filename, type);
  }
  /*
	public void setAssetMan(AssetManager assetMan) {
		this.assetMan = assetMan;
	}

	*/

	public ResourceManager(IResourceWatcher owner){
		assetMan = new AssetManager();
		postLoadList = new PooledLinkedList<IResourceUser>(100);
		addResourceUser(owner);
		this.owner = owner;
	}

	public boolean update(){
		if (loading){
			if (assetMan.update()) {
				loading = false;
				postLoadList.iterReverse();
				IResourceUser item;
				while((item = postLoadList.previous()) !=null) {
					item.postLoad(this);
					postLoadList.remove();
				}
				owner.loadComplete();
			}

			return false;
		}
		return true;
	}

	public boolean isLoading(){
		return loading;
	}
	
	public void dispose(){
		assetMan.dispose();
	}
	
	public void addResourceUser(IResourceUser object){
		postLoadList.add(object);
		object.preLoad(this);
		loading = true;
	}
	
	public void addResourceUser(IResourceUser[] objects){
		for(IResourceUser object : objects){
			addResourceUser(object);
		}
	}

}
