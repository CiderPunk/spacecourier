package net.ciderpunk.spacetrucker.sound;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.ObjectMap;
import net.ciderpunk.spacetrucker.resources.IResourceUser;
import net.ciderpunk.spacetrucker.resources.ResourceManager;

/**
 * Created by Matthew on 15/01/2016.
 */
public class SoundManager implements IResourceUser {

  public static final SoundManager instance = new SoundManager();

  public Sound[] explosions;
  public Sound[] scrapes;
  public Sound[] hits;
  public Sound[] shots;
  private SoundManager(){}

  public ObjectMap<String, Sound[]>  voices = new ObjectMap<String, Sound[]>();



  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.load("sound/explosion1.wav", Sound.class);
    resMan.load("sound/explosion2.wav", Sound.class);
    resMan.load("sound/explosion3.wav", Sound.class);
    resMan.load("sound/explosion4.wav", Sound.class);

    resMan.load("sound/hit1.wav", Sound.class);
    resMan.load("sound/hit2.wav", Sound.class);

    resMan.load("sound/scrape1.wav", Sound.class);
    resMan.load("sound/scrape2.wav", Sound.class);
    resMan.load("sound/scrape3.wav", Sound.class);
    resMan.load("sound/scrape4.wav", Sound.class);
    resMan.load("sound/scrape5.wav", Sound.class);

    resMan.load("sound/shoot.wav", Sound.class);

    loadVoices(resMan, "sound/voices/alien_a_", 5);
    loadVoices(resMan, "sound/voices/alien_b_", 9);
    loadVoices(resMan, "sound/voices/alien_c_", 6);

  }

  protected Sound[] loadVoices(ResourceManager resMan, String root, int count){
    Sound[] voices = new Sound[count];
    for (int i = 0; i < count; i++) {
       resMan.load(root + (i + 1) + ".ogg", Sound.class);
    }
    return voices;
  }

  protected Sound[] buildVoiceArray(ResourceManager resMan, String root, int count){
    Sound[] voices = new Sound[count];
    for (int i = 0; i < count; i++) {
      voices[i] = resMan.get(root + (i + 1) + ".ogg", Sound.class);
    }
    return voices;
  }


  @Override
  public void postLoad(ResourceManager resMan) {
    explosions = new Sound[]{
      resMan.get("sound/explosion1.wav", Sound.class),
      resMan.get("sound/explosion2.wav", Sound.class),
      resMan.get("sound/explosion3.wav", Sound.class),
      resMan.get("sound/explosion4.wav", Sound.class),
    };

    hits = new Sound[]{
      resMan.get("sound/hit1.wav", Sound.class),
      resMan.get("sound/hit2.wav", Sound.class)
    };

    scrapes = new Sound[]{
      resMan.get("sound/scrape1.wav", Sound.class),
      resMan.get("sound/scrape2.wav", Sound.class),
      resMan.get("sound/scrape3.wav", Sound.class),
      resMan.get("sound/scrape4.wav", Sound.class),
      resMan.get("sound/scrape5.wav", Sound.class)
    };

    shots = new Sound[]{
      resMan.get("sound/shoot.wav", Sound.class)
    };

    voices.put("lizard", buildVoiceArray(resMan, "sound/voices/alien_b_", 9));
    voices.put("brain", buildVoiceArray(resMan, "sound/voices/alien_a_", 5));
    voices.put("shroom", buildVoiceArray(resMan, "sound/voices/alien_c_", 6));
    voices.put("nose", buildVoiceArray(resMan, "sound/voices/alien_c_", 6));
    voices.put("sparks", buildVoiceArray(resMan, "sound/voices/alien_c_", 6));
    voices.put("explode", new Sound[]{resMan.get("sound/explosion1.wav", Sound.class)});

  }
}
